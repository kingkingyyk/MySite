package com.kingkingyyk.mysite.client;

import java.util.ArrayList;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;

@RemoteServiceRelativePath("greet")
public interface GreetingService extends RemoteService {
	ArrayList<String []> getPictureData() throws IllegalArgumentException;
	String getUVARankList(String username) throws IllegalArgumentException;
}
